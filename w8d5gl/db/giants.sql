CREATE TABLE giants(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	first_name VARCHAR(64),
	last_name VARCHAR(64),
	height INTEGER NOT NULL,
	date_of_birth TIMESTAMP,
	CHECK (height > 196)
);

INSERT INTO giants (first_name, last_name, height, date_of_birth)
VALUES ('Adam', 'Gibbons', 206, '1999-09-26'),
	('Sam', 'McArthur', 198, '2000-01-03'),
	('Brenden', 'Adams', 234, '1995-09-20'),
	('Lebron', 'James', 206, '1984-12-30'),
	('Rumeysa', 'Gelgi', 213, '1997-01-01');
	
	SELECT *
	FROM giants;