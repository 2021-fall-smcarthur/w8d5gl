package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Giant;

public interface GiantsService {
    
    List<Giant> getGiants();
}
