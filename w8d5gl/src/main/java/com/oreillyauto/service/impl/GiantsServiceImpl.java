package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.GiantsRepository;
import com.oreillyauto.domain.Giant;
import com.oreillyauto.service.GiantsService;

@Service
public class GiantsServiceImpl implements GiantsService {
    @Autowired
    GiantsRepository giantsRepo;
    
    @Override
    public List<Giant> getGiants() {
        // TODO Auto-generated method stub
        return  (List<Giant>) giantsRepo.findAll();
    }

    

}
