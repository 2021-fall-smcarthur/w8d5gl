package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.GiantsRepositoryCustom;
import com.oreillyauto.domain.Giant;

public interface GiantsRepository extends CrudRepository<Giant, String>, GiantsRepositoryCustom {
    // Spring Data abstract methods go here
    
  
    
    
}