package com.oreillyauto.dao.impl;

import java.util.List;


import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.GiantsRepositoryCustom;
import com.oreillyauto.domain.Giant;
import com.oreillyauto.domain.QGiant;

@Repository
public class GiantsRepositoryImpl extends QuerydslRepositorySupport implements GiantsRepositoryCustom {
    

    QGiant giantTable = QGiant.giant;
    
   

        public GiantsRepositoryImpl() {
        super(Giant.class);
        // TODO Auto-generated constructor stub
    }

        @SuppressWarnings("unchecked")
        @Override
        public List<Giant> getGiants() {
            String sql = "SELECT * " + 
                         "  FROM giants ";
            Query query = getEntityManager().createNativeQuery(sql);
            return (List<Giant>) query.getResultList();
            //return from(carpartTable).fetch();
        }

        @Override
        public Giant getGiantById(Integer idFind) {
            Giant giant = (Giant) getQuerydsl().createQuery()
                    .from(giantTable)
                    .where(giantTable.Id.eq(idFind))
                    .fetchOne();
                return giant;
        }
        
    }

