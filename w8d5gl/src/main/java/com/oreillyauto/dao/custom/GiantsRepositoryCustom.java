package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Giant;


public interface GiantsRepositoryCustom {
    public Giant getGiantById(Integer idFind);
    public List<Giant> getGiants();
}
