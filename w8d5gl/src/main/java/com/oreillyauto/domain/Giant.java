package com.oreillyauto.domain;

import java.io.Serializable;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GIANTS")
public class Giant implements Serializable {

    private static final long serialVersionUID = 4803117695298165720L;

    public Giant() {
    }
    
    public Giant(int Id, String first_name, String last_name, int height) {
        super();
        this.Id = Id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.height = height;
    }

    @Id
    @Column(name = "ID", columnDefinition = "INTEGER")
    private int Id;
    
    @Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String first_name;
    
    @Column(name = "last_name", columnDefinition = "VARCHAR(64)")
    private String last_name;
    
    @Column(name = "height", columnDefinition = "INTEGER")
    private int height;
    
    @Column(name = "date_of_birth", columnDefinition = "TIMESTAMP")
    private java.sql.Timestamp date_of_birth;
    
    public int getId() {
        return Id;
    }
    
    public void setId(int Id) {
        this.Id = Id;
    }
    
    public String getFirstName() {
        return first_name;
    }
    
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }
    
    public String getLastName() {
        return last_name;
    }
    
    public void setLastName(String last_name) {
        this.last_name = last_name;
    }
    
    public int getHeight() {
        return height;
    }
    
    public void setHeight(int height) {
        this.height = height;
    }
     
    
    public java.sql.Timestamp getDateOfBirth() {
        return date_of_birth;
    }
    
    public void setDateOfBirth(java.sql.Timestamp date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    @Override
    public String toString() {
        return "Giant [Id=" + Id + ", first_name=" + first_name + ", last_name=" + last_name + ", height=" + height + ", date_of_birth=" + date_of_birth + "]";
    }
    
}