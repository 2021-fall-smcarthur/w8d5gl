<%@ include file="/WEB-INF/layouts/include.jsp"%>
<c:url var="linkToHome" value="/" />

<div class="container-error">
	<h1>HTTP 500 - Internal Error</h1>
	<p>An internal exception has occurred. Please check your Eclipse console.</p>
</div>
